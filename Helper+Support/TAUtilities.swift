//
//  TAUtilities.swift
//  SampleAVPlayer
//
//  Created by Rahul Dubey on 03/01/22.
//

import Foundation


class TAUtility :NSObject {
    
    internal static let sharedInstance: TAUtility = {
        
        return TAUtility()
    }()
    
    
    
    class func showToast(message: String?) {

        DispatchQueue.main.async {

            if message != nil {

                if (message != "") && (message != "There seems to be a problem.Please try again later") {

                    ToastCenter.default.cancelAll()

                    ToastView.appearance().font = UIFont.systemFont(ofSize: 15)

                    Toast(text: message, duration: Delay.long).show()

                }

            }

        }

    }
}

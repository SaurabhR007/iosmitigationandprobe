//
//  ProbeVM.swift
//  SampleAVPlayer
//
//  Created by Rahul Dubey on 03/01/22.
//

import Foundation
//import ObjectMapper


class ProbeVM {
    
    func fetchDataWith<T: Decodable>(paramDict: [String: Any], skipLoader:Bool = false, urlEndpoint:String, requestType :KHTTPMethod, dataModelType: T.Type, completion: @escaping (T?,Bool) -> Void) {
        
//        if !skipLoader {
//            TAUtility.showLoader(message: kPleaseWait)
//        }
        
        TANetworkManager.sharedInstance.requestApi(serviceName: urlEndpoint, method: requestType, postData: paramDict) { (result, error) in
            
           // TAUtility.hideLoader()
            
            if error == nil {
                guard let responseJson = result else {
                    return
                }
                if let response: [String: Any] = responseJson as? [String: Any] {
                    if let type = response["type"] as? String {
                        if type == "success" {
                            if let resultDic = response["result"] as? [String:Any] {
                                guard let data = try? JSONSerialization.data(withJSONObject: resultDic, options: []) else {
                                    completion(nil,false)
                                    return
                                }
                                do {
                                    let parseData = try JSONDecoder().decode(dataModelType.self, from: data)
                                    completion(parseData,true)
                                }
                                catch let err {
                                    print("JSON PARSING ERROR: -\n \(err)")
                                    completion(nil,false)
                                }
                            }
                            else if let _ = response["result"] as? [Any] {
                                guard let data = try? JSONSerialization.data(withJSONObject: response, options: []) else {
                                    completion(nil,false)
                                    return
                                }
                                do {
                                    let parseData = try JSONDecoder().decode(dataModelType.self, from: data)
                                    completion(parseData,true)
                                }
                                catch let err {
                                    print("JSON PARSING ERROR: -\n \(err)")
                                    completion(nil,false)
                                }
                            }
                            else {
                                if let message: String = response["message"] as? String {
                                    TAUtility.showToast(message: message)
                                }
                                completion(nil, true)
                            }
                        } else {
                            if let message: String = response["message"] as? String {
                                TAUtility.showToast(message: message)
                            }
                            completion(nil, false)
                        }
                    }
                    else {
                        completion(nil, false)
                    }
                } else {
                    completion(nil, false)
                }
            } else {
                completion(nil, false)
            }
        }
    }
    
    func fetchDataWith(paramDict: [String: Any], skipLoader:Bool = false, urlEndpoint:String, requestType :KHTTPMethod, completion: @escaping ([String : Any]?,Bool) -> Void) {
        
        if !skipLoader {
           // TAUtility.showLoader(message: kPleaseWait)
        }
        
        TANetworkManager.sharedInstance.requestApi(serviceName: urlEndpoint, method: requestType, postData: paramDict) { (result, error) in
            
           // TAUtility.hideLoader()
            
            if error == nil {
                guard let responseJson = result else {
                    return
                }
                if let response: [String: Any] = responseJson as? [String: Any] {
                    if let type = response["type"] as? String {
                        if type == "success" {
                            if let resultDic = response["result"] as? [String:Any] {
                                completion(resultDic, true)
                            }
                            else if let items = response["result"] as? [Any] {
                                completion(["items" : items], true)
                            }
                            else {
                                if let message: String = response["message"] as? String {
                                    TAUtility.showToast(message: message)
                                }
                                completion(nil, true)
                            }
                        } else {
                            if let message: String = response["message"] as? String {
                                TAUtility.showToast(message: message)
                            }
                            completion(nil, false)
                        }
                    }
                    else {
                        TAUtility.showToast(message: "Operation Failed. Please check youyr internet connection and try again")
                        completion(nil, false)
                    }
                } else {
                    completion(nil, false)
                }
            } else {
                completion(nil, false)
            }
        }
    }

    
}

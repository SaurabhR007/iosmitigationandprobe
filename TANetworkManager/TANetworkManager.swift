//
//  TANetworkManager.swift
//  TANetworkingSwift

//  Created by Rahul Dubey on 03/01/22.
//  Copyright © 2022 Rahul Dubey. All rights reserved.
//


import UIKit
import Alamofire

public enum KHTTPMethod: String {
    case GET, POST, PUT, DELETE
}

public class TANetworkManager {
    
    // MARK: - Properties
    /**
     A shared instance of `Manager`, used by top-level Alamofire request methods, and suitable for use directly
     for any ad hoc requests.
     */
    internal static let sharedInstance: TANetworkManager = {
        let newtworkManager = TANetworkManager()
        newtworkManager.sessionManager = Session()
        newtworkManager.sessionManager?.session.configuration.timeoutIntervalForRequest = 20.0 //Seconds
        return newtworkManager
    }()
    
    private var sessionManager: Session?
    
    
    // MARK:- Public Method
    /**
     *  Initiates HTTPS or HTTP request over |KHTTPMethod| method and returns call back in success and failure block.
     *
     *  @param serviceName  name of the service
     *  @param method       method type like Get and Post
     *  @param postData     parameters
     *  @param responeBlock call back in block
     */
    

    internal func requestApi(serviceName: String, method: KHTTPMethod, postData: Parameters, skipBaseUrl: Bool = false, completionClosure: @escaping (_ result: Any?, _ error: Error?) -> ()) -> Void {
        
          //First Check here for internet Connection
        
        let dictParams = postData
        
        let serviceUrl = skipBaseUrl ? serviceName : (kBaseUrl + serviceName) //Rahuld
     
        print("TANetworkManager :: Service URL = \(serviceUrl)")
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictParams, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            let theJSONText = NSString(data: jsonData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            print("TANetworkManager :: JSON Param String = \(theJSONText!)")
            
        } catch let error as NSError {
            completionClosure(nil, error)
        }

        /*assert(method != .GET || method != .POST,
               "KHTTPMethod should be one of KHTTPMethodGET|KHTTPMethodPOST|KHTTPMethodPOSTMultiPart.")*/
        // Add AES authentication ...........
        let headers: HTTPHeaders = getHeaderWithAPIName(serviceName: serviceName, method: method)
        
        print("TANetworkManager :: Headers = \(headers)")
        var methodType = HTTPMethod.get
        var encoding: ParameterEncoding = JSONEncoding.default
        
        switch method {
        case .POST:
            methodType = .post
            encoding = JSONEncoding.default
        case .PUT:
            methodType = .put
            encoding = JSONEncoding.default
        case .DELETE:
            methodType = .delete
            encoding = JSONEncoding.default
        default:
            methodType = .get
            encoding = URLEncoding.queryString
        }
        
        sessionManager?.request(serviceUrl, method: methodType, parameters: dictParams, encoding: encoding, headers: headers ).responseJSON  { (response) in
           // TAUtility.hideLoader()
            DispatchQueue.main.async {
                switch response.result {
                case .success:
                    print("TANetworkManager :: Success with JSON: \n")
                    guard let data = response.data else {
                        return
                    }
                    if let jsonString = String(data: data, encoding: .utf8) {
                        print(jsonString)
                    }
                    
                    do {
                        let anyObj = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: Any]
                        print("TANetworkManager :: Success with Result -> \(anyObj!)")
                        if let res = anyObj {
                        completionClosure(anyObj, nil)
                        }
                    } catch let error as NSError {
                        completionClosure(nil, error)
                        
                        print("TANetworkManager :: Json Error: \(error.localizedDescription)")
                    }
                    
                case .failure(let error):

                    switch error {
                        
                        case .sessionTaskFailed(let urlError as URLError) where urlError.code == .timedOut:
                            
                            TAUtility.showToast(message: "Operation Failed.Please check your internet connection and try again")
                           
                        default:
                            print("TANetworkManager :: Failure with Error: \(error.localizedDescription)")
                           // TAUtility.showToast(message: "Operation Failed.Please check your internet connection and try again")
                    }
                    completionClosure(nil, error)
                }
            }
        }
    }
    
    // MARK: - Private Method
    private func getHeaderWithAPIName(serviceName: String, method: KHTTPMethod) -> HTTPHeaders {
        let headers: HTTPHeaders
        headers = ["Content-Type": "application/json",
                   "Accept": "application/json"
        ]
        return headers
    }

}

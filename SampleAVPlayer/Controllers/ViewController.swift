//
//  ViewController.swift
//  SampleAVPlayer
//
//  Created by Saurabh Rode on 21/12/21.
//

import UIKit
import AVFoundation
import AVKit


enum BufferCategory{
    case defaultBuffer
    case startUpBuffer
    case reBuffer
    
    static var currentBufferState:BufferCategory = .startUpBuffer
}


class ViewController: UIViewController {

    @IBOutlet weak var labelForAvailableBuffer: UILabel!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var bitrateLabel: UILabel!
    @IBOutlet weak var resolutionLabel: UILabel!
    @IBOutlet weak var bufferDuration: UILabel!
    @IBOutlet weak var reBufferDurationLabel: UILabel!
    
    @IBOutlet weak var bitrateSlider: UISlider!
    @IBOutlet weak var resolutionSlider: UISlider!
    @IBOutlet weak var bufferDurationSlider: UISlider!
    @IBOutlet weak var reBufferDurationSlider: UISlider!
    @IBOutlet weak var progressSlider: BufferSlider!
    @IBOutlet weak var lblCureentTime: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    
    private var loadedTimeRanges: NSKeyValueObservation?
    private var playerItemBufferEmptyObserver: NSKeyValueObservation?
    private var playerItemBufferKeepUpObserver: NSKeyValueObservation?
    private var playerItemBufferFullObserver: NSKeyValueObservation?
    private var player:AVPlayer?
    private var playerItem:AVPlayerItem?
    let urlString = "https://dvnasw5fy1lpp.cloudfront.net/shemaroo-vod/altplatform/KissebaazFUMHDmp4-247/main.m3u8"
        //"https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8"
        //"http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8"
    //
       
    
    var parsedResponse:[Any]?
    var availableBandWidth = [String]()
    var availableResoltuons = [String]()
    var hlsParser =  HLSParser()
    
    var probeInterface:ProbeInterface?
    
    var updater : CADisplayLink! = nil
    
    var isResolutionValueChanged = false
    var isBitrateValueChanged = false
    
    var reBufferDurationSliderValue:Float = 0.0
    var startUpBufferValue:Float = 0.0
    var defaultBufferValue:Float = 5.0
    var codec =  ""
    private func setupObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(willResignActive), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    
    @objc
    private func willResignActive() {
        player?.pause()
    }

    @objc
    private func didBecomeActive() {
        player?.play()
    }

    
    @IBAction func rebufferingDurationValueChanged(_ sender: Any) {
        ProbeInterface.setUserInfo(ueid:"saurabhrode@gmail.com")
        self.probeInterface?.sendEvent(eventType: .BUFFERING)
        self.reBufferDurationLabel.text = String(self.reBufferDurationSlider.value)
        reBufferDurationSliderValue = self.reBufferDurationSlider.value
//        if self.player?.rate == 1{
//            self.player?.currentItem?.preferredForwardBufferDuration = TimeInterval(Int(self.reBufferDurationSlider.value ))
//        }
    }
    
    
    @IBAction func bitrateValueChanged(_ sender:UISlider) {
        if self.isResolutionValueChanged{
            self.bitrateSlider.value = sender.value
            self.isResolutionValueChanged = false
        }else{
            self.isBitrateValueChanged = true
            self.resolutionsValueChanged(sender)
        }
        self.bitrateLabel.text =  self.availableBandWidth[Int(sender.value)-1]
        playerItem?.preferredPeakBitRate = Double(self.availableBandWidth[Int(sender.value)-1]) ?? 0
    }

    
    @IBAction func resolutionsValueChanged(_ sender: UISlider) {
        
        if self.isBitrateValueChanged {
            self.resolutionSlider.value = sender.value
            self.isBitrateValueChanged = false
        }else{
            self.bitrateValueChanged(sender)
        }
        
        self.resolutionLabel.text = self.availableResoltuons[Int(sender.value)-1]
        let value: String = self.availableResoltuons[Int(sender.value)-1]
        let resolution = value.components(separatedBy: "x")

        let width: CGFloat = resolution[0].CGFloatValue() ?? 0
        let height: CGFloat = resolution[1].CGFloatValue() ?? 0
        
        print("\(width)     \(height)  ")
        
        self.playerItem?.preferredMaximumResolution = CGSize(width: width, height: height)
        self.isResolutionValueChanged = true
    
    }
    
    @IBAction func bufferDurationValueChanged(_ sender: UISlider) {
        self.bufferDuration.text = String(self.bufferDurationSlider.value)
        self.player?.currentItem?.preferredForwardBufferDuration = TimeInterval(Int(self.bufferDurationSlider.value ))
       
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Timer.scheduledTimer(withTimeInterval: 0.3, repeats: true) { (Timer) in
            self.labelForAvailableBuffer.text = "Pref Buf " + String(self.player?.currentItem?.preferredForwardBufferDuration ?? 0)
        }
      
        progressSlider.isContinuous = false
        progressSlider.addTarget(self, action: #selector(self.playbackSliderValueChanged), for: .valueChanged)
       
        self.configUI()
        hlsParser.parseStreamTags(link:urlString) { [weak self] (dataArray, data,bitaratArray, resolutionArray,codec)   in
            
            DispatchQueue.main.async {
                debugPrint(dataArray[0])
                self?.availableBandWidth = bitaratArray
                self?.availableBandWidth.reverse()
                self?.bitrateLabel.text = self?.availableBandWidth[0]
                self?.availableResoltuons = resolutionArray
                self?.availableResoltuons.reverse()
                self?.parsedResponse = dataArray
                self?.bitrateSlider.maximumValue = Float(self?.availableBandWidth.count ?? 0)
                self?.bitrateSlider.minimumValue = 1
                self?.resolutionSlider.maximumValue = Float(self?.availableResoltuons.count ?? 0)
                if resolutionArray.count > 0{
                   self?.resolutionLabel.text = self?.availableResoltuons[0] ?? ""
                }
                
                self?.resolutionSlider.minimumValue = 1
                self?.codec = codec
            }
            
        } failedBlock: { _ in
            
        }
        self.player?.currentItem?.preferredForwardBufferDuration = TimeInterval(Int(self.bufferDurationSlider.value ))
    }
    
    @objc func playbackSliderValueChanged(playbackSlider:UISlider, event: UIEvent) {
        self.player?.currentItem?.preferredForwardBufferDuration = TimeInterval(Int(self.reBufferDurationSliderValue))
        
        Toast1.show(message:"Rebuffer\(self.player?.currentItem?.preferredForwardBufferDuration)", controller: self)
              let seconds : Int64 = Int64(self.progressSlider.value)
              let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
              self.player!.seek(to: targetTime)
              if let touchEvent = event.allTouches?.first {
                  switch touchEvent.phase {
                  
                  case .began:
                      //self.removeTimerObserver()
                     // self.pauseAudio()
                      player?.pause()
                      break
                    
                  case .moved:
                      break

                  case .ended:
                      //self.timeObserverSetup()
                     // self.playAudio()
                      player?.play()
                      break

                  default:
                      break

                  }
              }
      }
    
    
    func configUI(){
        self.bufferDurationSlider.value = 5
        self.bufferDurationSlider.minimumValue = 1
        self.bufferDurationSlider.maximumValue = 60
        self.reBufferDurationSlider.value = 20
        self.reBufferDurationSlider.minimumValue = 1
        self.reBufferDurationSlider.maximumValue = 60
        self.bufferDuration.text = String(self.bufferDurationSlider.value)
        self.reBufferDurationLabel.text = String(self.reBufferDurationSlider.value)
        self.progressSlider.setBufferValue(0, animated: false)
        reBufferDurationSliderValue = self.reBufferDurationSlider.value
    }

    func setUpPlayer(){
        let url = URL(string: urlString)
        self.playerItem =   AVPlayerItem.init(url: url!)
        self.player = AVPlayer.init(playerItem: playerItem)
        self.player?.addObserver(self, forKeyPath: "rate", options: NSKeyValueObservingOptions.new, context: nil)
        player?.automaticallyWaitsToMinimizeStalling = false
        self.player?.currentItem?.preferredForwardBufferDuration = TimeInterval(Int(self.bufferDurationSlider.value))
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "rate" {
            if player?.rate ?? 0 > 0 {
                if BufferCategory.currentBufferState == .startUpBuffer{
                    Toast1.show(message:"Default Buffer", controller: self)
                    self.player?.currentItem?.preferredForwardBufferDuration = TimeInterval(Int(self.defaultBufferValue))
                    BufferCategory.currentBufferState = .defaultBuffer
                }
            }
        }
    }
    
    @IBAction func btnTaped(_ sender: Any) {
        if((self.player?.isPlaying) != nil){
            self.bufferDurationSlider.isEnabled = true
            player?.pause() // 1. pause the player to stop it
            player = nil
            playBtn.setTitle("Play", for: .normal)
        }else{
            self.bufferDurationSlider.isEnabled = false
            self.setUpPlayer()
            playVideos()
        
            self.ConfigureProbe()
            playBtn.setTitle("Stop", for: .normal)
        }
    }
    
    func ConfigureProbe(){
        self.probeInterface = ProbeInterface(player: self.player, requiredInfo:EventModel(sessionID:"xxx-yyy-zzz", playerApp:"AVPlayer", provider:"HotStar", videoID:"123", has:""))
        self.probeInterface?.configureProbe(videoQuality: true, bitrate: true, stalledCount: true)
    }
    
    func observeBuffering() {
        
        playerItemBufferEmptyObserver = player?.currentItem?.observe(\AVPlayerItem.isPlaybackBufferEmpty, options: [.new]) { [weak self] (_, _) in
            print("Saurabh Player playerItemBufferEmptyObserver")
            BufferCategory.currentBufferState = .reBuffer
                if let value = self?.reBufferDurationSlider.value{
                    Toast1.show(message:"Rebuffer \(value)", controller: self!)
                }
                self?.player?.currentItem?.preferredForwardBufferDuration = TimeInterval(Int(self?.reBufferDurationSliderValue ?? 5))
           
        }
            
        playerItemBufferKeepUpObserver = player?.currentItem?.observe(\AVPlayerItem.isPlaybackLikelyToKeepUp, options: [.new]) { [weak self] (_, _) in
         //   if self?.player?.rate == 0 {
               //self?.player?.play()
         //   }
          
            print("Saurabh Player playerItemBufferKeepUpObserver")
            
        }
            
        playerItemBufferFullObserver = player?.currentItem?.observe(\AVPlayerItem.isPlaybackBufferFull, options: [.new]) { [weak self] (_, _) in
            
//            if self?.player?.rate == 0 {
               self?.player?.play()
           // }
            if BufferCategory.currentBufferState == .reBuffer{
                Toast1.show(message:"Default Buffer", controller: self!)
                self?.player?.currentItem?.preferredForwardBufferDuration = TimeInterval(Int(self?.defaultBufferValue ?? 100))
                BufferCategory.currentBufferState = .defaultBuffer
                print("Saurabh playerItemBufferFullObserver")
              
            }
           
        }
    }
    
    func secondsToHoursMinutesSeconds(_ seconds: Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func playVideos() {
 
    
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.playerView.bounds
        self.player?.currentItem?.preferredPeakBitRate = Double(self.availableBandWidth[Int(self.bitrateSlider.value - 1)]) ?? 0

        self.playerView.layer.addSublayer(playerLayer)
        player?.play()
        let bufferTimeValue = Float(CMTimeGetSeconds(self.availableDuration()) / CMTimeGetSeconds((self.player?.currentItem!.duration)!))
        guard let duration : CMTime = self.player?.currentItem?.asset.duration  else {return}
        let seconds : Float64 = CMTimeGetSeconds(duration)
        self.progressSlider.maximumValue = Float(seconds)
        
        
        player?.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: Int32(NSEC_PER_SEC)), queue: .main) { time in
            print("Buffer available  \(CMTimeGetSeconds(self.availableDuration())) ********VS******** \(Float(CMTimeGetSeconds(time)))")
            guard let duration : CMTime = self.player?.currentItem?.asset.duration  else {return}
            let seconds : Float64 = CMTimeGetSeconds(duration)
            let currentTime : CMTime = self.player!.currentTime()
            let currentTimeSeconds : Float64 = CMTimeGetSeconds(currentTime)
            
            self.progressSlider.value = Float(CMTimeGetSeconds(time))
            self.lblCureentTime.text = self.stringFromTimeInterval(interval: currentTimeSeconds)
            self.lblDuration.text = self.stringFromTimeInterval(interval: seconds)
            let changedBuffer =  CGFloat(CMTimeGetSeconds(self.availableDuration()))
            self.progressSlider.setBufferValue(changedBuffer/CGFloat(seconds), animated: false)

            }
            self.observeBuffering()
        
    }
    
    public var bufferAvail: TimeInterval {

        // Check if there is a player instance
        if ((player?.currentItem) != nil) {

            // Get current AVPlayerItem
            let item: AVPlayerItem = (player?.currentItem)!
            if (item.status == AVPlayerItem.Status.readyToPlay) {

                let timeRangeArray: NSArray = item.loadedTimeRanges as NSArray
                let aTimeRange: CMTimeRange = (timeRangeArray.object(at: 0) as AnyObject).timeRangeValue
                let startTime = CMTimeGetSeconds(aTimeRange.start)
                let loadedDuration = CMTimeGetSeconds(aTimeRange.duration)

                return (TimeInterval)(startTime + loadedDuration);
            }
            else {
                return(CMTimeGetSeconds(CMTime.invalid))
            }
        }
        else {
            return(CMTimeGetSeconds(CMTime.invalid))
        }
    }
    
    func availableDuration() -> CMTime
    {
        if let range = self.player?.currentItem?.loadedTimeRanges.first {
            return CMTimeRangeGetEnd(range.timeRangeValue)
        }
        return .zero
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> String {

        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        let hours = (interval / 3600)
       /*
        print("Rahuld",hours,minutes,seconds)
        
        if hours > 0 {
            
          return String(format: "%02d:%02d:%02d", hours, minutes, seconds)

            
        }else if minutes > 0 {
            
            return String(format: "%02d:%02d", minutes, seconds)

            
            
        }else if seconds > 0 {
            
            return String(format: "%02d:%02d", minutes, seconds)

        }else {
            
            return String(format: "%02d:%02d:%02d", hours, minutes, seconds)

            
        }*/
        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)

    }
    

}



public extension AVPlayerItem {

    func totalBuffer() -> Double {
        return self.loadedTimeRanges
            .map({ $0.timeRangeValue })
            .reduce(0, { acc, cur in
                return acc + CMTimeGetSeconds(cur.start) + CMTimeGetSeconds(cur.duration)
            })
    }

    func currentBuffer() -> Double {
        let currentTime = self.currentTime()

        guard let timeRange = self.loadedTimeRanges.map({ $0.timeRangeValue })
            .first(where: { $0.containsTime(currentTime) }) else { return -1 }

        return CMTimeGetSeconds(timeRange.end) - currentTime.seconds
    }
    
    

}


public struct HLSParser {
    
    public typealias success = (_ parsedResponse:[Any], _ data:Data?,_ birateArray:[String],_ resolutionArray:[String],_ codec:String) -> Void
    public typealias failed = (_ error:Error?) -> Void
 
    
    public init() {}
        
    public mutating func parseStreamTags(link: String,successBlock: @escaping success, failedBlock:@escaping failed) {
        var request = URLRequest(url: URL(string: link)!)
        request.httpMethod = "Get"
        var resultArr = [Any]()
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            // Check if an error occured
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                failedBlock(error) // return data & close
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            
            let tmpStr = "#EXT-X-STREAM-INF:"
            let ArrayCop = responseString?.components(separatedBy: tmpStr)
            var croppedArr =  [Any]()
            croppedArr =  Array((ArrayCop?.dropFirst())!)
            let enterChar = "\n"
            let spaceChar = ","
            
            var bitrateArray = [String] ()
            var resolutionaArray = [String]()
            var codec1 = ""
            for i in 0..<(croppedArr.count) {
                var parsedDic = [String:Any]()
                let tmpArr1 =  ((croppedArr[i] as! String).trimmingCharacters(in: .whitespacesAndNewlines) ).components(separatedBy: enterChar)
                parsedDic["LINK"] = tmpArr1.last!
                let finalStr = tmpArr1[0]
                let arrayofCom = finalStr.components(separatedBy: spaceChar)
                for ind in 0..<arrayofCom.count {
                    if arrayofCom[ind].contains("BANDWIDTH=") {
                        parsedDic["BANDWIDTH"] = arrayofCom[ind].dropFirst(10)
                        bitrateArray.append(String(arrayofCom[ind].dropFirst(10)))
                      
                        
                    }
                    if arrayofCom[ind].contains("RESOLUTION=") {
                        parsedDic["RESOLUTION"] = arrayofCom[ind].dropFirst(11)
                        resolutionaArray.append(String(arrayofCom[ind].dropFirst(11)))
                       
                    }
                    
                    if arrayofCom[ind].contains("CODECS=") {
                        parsedDic["CODECS"] = arrayofCom[ind].dropFirst(8)
                        codec1 = String(arrayofCom[ind].dropFirst(8))
                        
                    }
                }
                resultArr.append(parsedDic)
            }
            successBlock(resultArr,data,bitrateArray,resolutionaArray, codec1)
            
        }
        
        task.resume()
    }
    
}

extension String {

  func CGFloatValue() -> CGFloat? {
    guard let doubleValue = Double(self) else {
      return nil
    }

    return CGFloat(doubleValue)
  }
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }

}


class Toast1 {
    static func show(message: String, controller: UIViewController) {
        let toastContainer = UIView(frame: CGRect())
        toastContainer.backgroundColor = UIColor.black.withAlphaComponent(1)
        toastContainer.alpha = 0.0
        toastContainer.layer.cornerRadius = 25;
        toastContainer.clipsToBounds  =  true

        let toastLabel = UILabel(frame: CGRect())
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font.withSize(12.0)
        toastLabel.text = message
        toastLabel.clipsToBounds  =  true
        toastLabel.numberOfLines = 0

        toastContainer.addSubview(toastLabel)
        controller.view.addSubview(toastContainer)
        

        toastLabel.translatesAutoresizingMaskIntoConstraints = false
        toastContainer.translatesAutoresizingMaskIntoConstraints = false

        let a1 = NSLayoutConstraint(item: toastLabel, attribute: .leading, relatedBy: .equal, toItem: toastContainer, attribute: .leading, multiplier: 1, constant: 15)
        let a2 = NSLayoutConstraint(item: toastLabel, attribute: .trailing, relatedBy: .equal, toItem: toastContainer, attribute: .trailing, multiplier: 1, constant: -15)
        let a3 = NSLayoutConstraint(item: toastLabel, attribute: .bottom, relatedBy: .equal, toItem: toastContainer, attribute: .bottom, multiplier: 1, constant: -15)
        let a4 = NSLayoutConstraint(item: toastLabel, attribute: .top, relatedBy: .equal, toItem: toastContainer, attribute: .top, multiplier: 1, constant: 15)
        toastContainer.addConstraints([a1, a2, a3, a4])

        let c1 = NSLayoutConstraint(item: toastContainer, attribute: .leading, relatedBy: .equal, toItem: controller.view, attribute: .leading, multiplier: 1, constant: 65)
        let c2 = NSLayoutConstraint(item: toastContainer, attribute: .trailing, relatedBy: .equal, toItem: controller.view, attribute: .trailing, multiplier: 1, constant: -65)
        let c3 = NSLayoutConstraint(item: toastContainer, attribute: .bottom, relatedBy: .equal, toItem: controller.view, attribute: .bottom, multiplier: 1, constant: -75)
        controller.view.addConstraints([c1, c2, c3])

        UIView.animate(withDuration: 2, delay: 0.0, options: .curveEaseIn, animations: {
            toastContainer.alpha = 1.0
        }, completion: { _ in
            UIView.animate(withDuration: 0.5, delay: 1.5, options: .curveEaseOut, animations: {
                toastContainer.alpha = 0.0
            }, completion: {_ in
                toastContainer.removeFromSuperview()
            })
        })
    }
}


extension AVPlayer {



    func addProgressObserver(action:@escaping ((Double) -> Void)) -> Any {

        return self.addPeriodicTimeObserver(forInterval: CMTime.init(value: 1, timescale: 1), queue: nil, using: { time in
            let progress = Float(CMTimeGetSeconds(self.availableDuration()))
            action(Double(progress))
            
        })
        
    }

    func availableDuration() -> CMTime{
        if let range = self.currentItem?.loadedTimeRanges.first {
            return CMTimeRangeGetEnd(range.timeRangeValue)
        }
      return .zero

    }



}

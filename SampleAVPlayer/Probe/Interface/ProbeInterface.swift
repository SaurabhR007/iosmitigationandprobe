//
//  ProbeInterface.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 29/09/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
import UIKit
import AVKit

public class ProbeInterface:NSObject,ProbeProtocol{
    private weak var player:AVPlayer?
    private var lastIndicatedBitrate:Double = 1
    private var dataHandler:ProbeDataHandler? = ProbeDataHandler()
    private var currentBufferStatus:ProbePlayerStatus = .bufferEnd
    private var stallStartTime:Int?
    private var playBackTime:Int = -2
    
    static func setUserInfo(ueid:String){
        UserDefaults.standard.setValue(ueid, forKey:ProbeUserDefaults.ueid.rawValue)
    }
    
    
    init(player:AVPlayer?,requiredInfo:EventModel) {
        super.init()
        self.player = player
        var pingModel = ProbePingModel()
        var eventModel = ProbeEventModel()
        let sessionID = requiredInfo.sessionID
        let playerApp = requiredInfo.playerApp
        let provider = requiredInfo.provider
        let videoID = requiredInfo.videoID
        let has = requiredInfo.has
        pingModel.ping = PINGModel(sessionID: sessionID , playerApp: playerApp, provider: provider, videoID: videoID, has: has)
        eventModel.event = requiredInfo
        self.dataHandler?.probePingDataSource = pingModel
        self.dataHandler?.probeEventDataSource = eventModel
        self.dataHandler?.delegate = self
        let interval = CMTimeMake(value:10, timescale:10)
        self.player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { [weak self] time in
            if self?.player?.rate == 1{
                self?.playBackTime = self!.playBackTime + 1
                debugPrint("#Probe playBackTime  \(String(describing: self?.playBackTime))")
            }
        })
    }
    
    public func configureProbe(videoQuality:Bool,bitrate:Bool,stalledCount:Bool){
        debugPrint("#Probe Configured")
        if videoQuality {
            self.videoQualitySwitchObserver()
        }
        if bitrate {
            NotificationCenter.default.addObserver(self, selector: #selector(bitrateSwitchObserver(notification:)), name: NSNotification.Name(NSNotification.Name.AVPlayerItemNewAccessLogEntry.rawValue), object: nil)
        }
        if stalledCount{
            let center = NotificationCenter.default
            center.addObserver(self, selector: #selector(handlePlaybackStalled(_:)), name: Notification.Name.AVPlayerItemPlaybackStalled, object: nil)
            self.player?.currentItem?.addObserver(self, forKeyPath: ProbePlayerPlaybackLikelyToKeepUp, options: .new, context: nil)
        }
    }
    
    public func sendEvent(eventType:ProbeEventType){
        self.dataHandler?.probeEventDataSource.event?.event = eventType.rawValue
        self.dataHandler?.syncEventsWithServer()
    }
    
    @objc func handlePlaybackStalled(_ notification: Notification) {
        if self.currentBufferStatus != .playbackStalled{
            if let value = self.dataHandler?.probePingDataSource.ping?.stall?.count{
                self.dataHandler?.probePingDataSource.ping?.stall?.count = value + 1
            }else{
                self.dataHandler?.probePingDataSource.ping?.stall?.count = 1
            }
            if let _ = self.dataHandler?.probePingDataSource.ping?.stall?.duration{
            }else{
                self.dataHandler?.probePingDataSource.ping?.stall?.duration = 0
            }
            self.currentBufferStatus = .playbackStalled
            self.stallStartTime = NSDate.currentEpochTime
        }
    }

    public func stopProbeInterface(){
        self.player = nil
        self.lastIndicatedBitrate = 0
        self.dataHandler = nil
        self.dataHandler = ProbeDataHandler()
    }
    
    @objc func bitrateSwitchObserver(notification: Notification){
        debugPrint("#Probe bitrateSwitchObserver")
        guard let playerItem = notification.object as? AVPlayerItem,
              let lastEvent = playerItem.accessLog()?.events.last else {
            return
        }
        let currentIndicatedBitrate = lastEvent.observedBitrate
        if self.lastIndicatedBitrate != currentIndicatedBitrate {
            debugPrint("#Probe bitrateSwitchObserver bitrate changed from %d to %d ###",self.lastIndicatedBitrate,currentIndicatedBitrate )
            if currentIndicatedBitrate > self.lastIndicatedBitrate{
                self.dataHandler?.probePingDataSource.ping?.pingSwitch?.up = [String(NSDate.currentEpochTime):Int(currentIndicatedBitrate )]
            }else{
                self.dataHandler?.probePingDataSource.ping?.pingSwitch?.down = [String(NSDate.currentEpochTime):Int(currentIndicatedBitrate )]
            }
            self.lastIndicatedBitrate = currentIndicatedBitrate
        }
    }
    
    private func videoQualitySwitchObserver(){
        self.player?.addObserver(self, forKeyPath:ProbeKeyPaths.videoQuality.rawValue, options: [.old, .new], context: nil)
    }
    
    public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        switch keyPath {
        case ProbePlayerPlaybackLikelyToKeepUp:
            if currentBufferStatus == .playbackStalled{
                let endTime = NSDate.currentEpochTime
                let startTime = self.stallStartTime ?? 0
                let duration = NSDate.getDurationInSeconds(startTime: TimeInterval.init(startTime), endTime:TimeInterval.init(endTime))
                if let value = self.dataHandler?.probePingDataSource.ping?.stall?.duration{
                    self.dataHandler?.probePingDataSource.ping?.stall?.duration = value + duration
                }else{
                    self.dataHandler?.probePingDataSource.ping?.stall?.duration = duration
                }
                self.currentBufferStatus = .bufferEnd
            }
       
        case ProbeKeyPaths.videoQuality.rawValue:
        break
        default: break
        }
    }
}

extension ProbeInterface{
    
    func setTimeStamp(isEvent: Bool){
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.timestamp =  NSDate.currentEpochTime
        }else{
            self.dataHandler?.probePingDataSource.ping?.timestamp =   NSDate.currentEpochTime
        }
    }
    
    func setFrameRate(isEvent: Bool) {
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.frameRate =  self.player?.currentFrameRate
        }else{
            self.dataHandler?.probePingDataSource.ping?.frameRate =  self.player?.currentFrameRate
        }
    }
    
    func setPlayBackPostion(isEvent: Bool) {
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.playbackPosInSEC = self.player?.playerCurrentTimeInSec
        }else{
            self.dataHandler?.probePingDataSource.ping?.playbackPosInSEC = self.player?.playerCurrentTimeInSec
        }
        
    }
    
    func setCodecType(isEvent: Bool) {
        let value = self.player?.tracks()
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.aCodec = value?.1
            self.dataHandler?.probeEventDataSource.event?.vCodec = value?.0
        }else{
            self.dataHandler?.probePingDataSource.ping?.aCodec = value?.1
            self.dataHandler?.probePingDataSource.ping?.vCodec = value?.0
        }
      
    }
    
    func setDRMType(isEvent: Bool){
        let value = self.player?.currentItem?.asset.hasProtectedContent ?? false
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.drm = value ? "true" : "false"
        }else{
            self.dataHandler?.probePingDataSource.ping?.drm = value ? "true" : "false"
        }
    
    }
    
    func setAssetDuration(isEvent: Bool){
        let value = self.player?.videoDuration
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.assetDuration = Int(value ?? 0)
        }else{
            self.dataHandler?.probePingDataSource.ping?.assetDuration = Int(value ?? 0)
        }
       
    }
    
    func setResolution(isEvent: Bool){
        let value = self.player?.currentResolution
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.resolution = value
        }else{
            self.dataHandler?.probePingDataSource.ping?.resolution = value
        }
       
    }
    
    func setThroughPut(isEvent: Bool){
        let value = self.player?.throughPut
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.throughput = value
        }else{
            self.dataHandler?.probePingDataSource.ping?.throughput = value
        }
    
    }
    
    func setBitrate(isEvent: Bool){
        let value = self.player?.currentBitRate
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.bitrate = value
        }else{
            self.dataHandler?.probePingDataSource.ping?.bitrate = value
        }
       
    }
        
    func setLiveVideo(isEvent: Bool){
        let value = self.player?.isLiveVideoPlaying ?? false ? "true" : "false"
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.live = value
        }else{
            self.dataHandler?.probePingDataSource.ping?.live = value
        }
    }
    
    func setIP(isEvent: Bool){
        let value = UIDevice.getIP()
        print("Probe IP \(String(describing: UIDevice.getIP()))")
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.ip = value?.MD5
        }else{
            self.dataHandler?.probePingDataSource.ping?.ip = value?.MD5
        }
    }
    
    func setUeid(isEvent:Bool){
        let value = UserDefaults.standard.value(forKey: ProbeUserDefaults.ueid.rawValue) as? String
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.ueid = value?.MD5
            
        }else{
            self.dataHandler?.probePingDataSource.ping?.ueid = value?.MD5
        }
    }
    
    func setDurationOfPlayBack(isEvent:Bool){
        if !isEvent{
            self.dataHandler?.probePingDataSource.ping?.durationOfPlayback = self.playBackTime
        }
        
    }
    
}


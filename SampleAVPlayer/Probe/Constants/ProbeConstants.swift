//
//  Probe_Constants.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 28/09/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

let ProbePlayerPlaybackLikelyToKeepUp = "playbackLikelyToKeepUp"

enum ProbeKeyPaths:String {
    case videoQuality = "currentItem.presentationSize"
    case videoRate = "rate"
}

enum ProbeUserDefaults:String{
    case ueid = "ueid"
}

enum ProbeMetrices {
    case videoQuality
}

public enum ProbePlayerStatus: Int {
    case playbackStalled
    case bufferBegin
    case bufferEnd
}

public enum ProbeEventType: String {
    case START = "START"
    case STOP = "STOP"
    case SEEK = "SEEK"
    case BUFFERING = "BUFFERING"
    
    static func value(event:ProbeEventType)->String{
        return event.rawValue
    }
}

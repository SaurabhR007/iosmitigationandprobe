//
//  ProbeServiceHandler.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 01/10/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

//ec2-18-212-51-25.compute-1.amazonaws.com:8080/capture



class ProbeServiceHandler {
    
    func CallApiService<T: Decodable>(paramDict: [String: Any], skipLoader:Bool = false, urlEndpoint:String, requestType :KHTTPMethod, dataModelType: T.Type, completion: @escaping (T?,Bool) -> Void) {
        
//        if !skipLoader {
//            TAUtility.showLoader(message: kPleaseWait)
//        }
        
        TANetworkManager.sharedInstance.requestApi(serviceName: urlEndpoint, method: requestType, postData: paramDict) { (result, error) in
            
           // TAUtility.hideLoader()
            
            if error == nil {
                guard let responseJson = result else {
                    return
                }
                if let response: [String: Any] = responseJson as? [String: Any] {
                    if let type = response["type"] as? String {
                        if type == "success" {
                            if let resultDic = response["result"] as? [String:Any] {
                                guard let data = try? JSONSerialization.data(withJSONObject: resultDic, options: []) else {
                                    completion(nil,false)
                                    return
                                }
                                do {
                                    let parseData = try JSONDecoder().decode(dataModelType.self, from: data)
                                    completion(parseData,true)
                                }
                                catch let err {
                                    print("JSON PARSING ERROR: -\n \(err)")
                                    completion(nil,false)
                                }
                            }
                            else if let _ = response["result"] as? [Any] {
                                guard let data = try? JSONSerialization.data(withJSONObject: response, options: []) else {
                                    completion(nil,false)
                                    return
                                }
                                do {
                                    let parseData = try JSONDecoder().decode(dataModelType.self, from: data)
                                    completion(parseData,true)
                                }
                                catch let err {
                                    print("JSON PARSING ERROR: -\n \(err)")
                                    completion(nil,false)
                                }
                            }
                            else {
                                if let message: String = response["message"] as? String {
                                    TAUtility.showToast(message: message)
                                }
                                completion(nil, true)
                            }
                        } else {
                            if let message: String = response["message"] as? String {
                                TAUtility.showToast(message: message)
                            }
                            completion(nil, false)
                        }
                    }
                    else {
                        completion(nil, false)
                    }
                } else {
                    completion(nil, false)
                }
            } else {
                completion(nil, false)
            }
        }
    }
    
    func fetchDataWith(paramDict: [String: Any], skipLoader:Bool = false, urlEndpoint:String, requestType :KHTTPMethod, completion: @escaping ([String : Any]?,Bool) -> Void) {
        
        if !skipLoader {
           // TAUtility.showLoader(message: kPleaseWait)
        }
        
        TANetworkManager.sharedInstance.requestApi(serviceName: urlEndpoint, method: requestType, postData: paramDict) { (result, error) in
            
           // TAUtility.hideLoader()
            
            if error == nil {
                guard let responseJson = result else {
                    return
                }
                if let response: [String: Any] = responseJson as? [String: Any] {
                    if let type = response["type"] as? String {
                        if type == "success" {
                            if let resultDic = response["result"] as? [String:Any] {
                                completion(resultDic, true)
                            }
                            else if let items = response["result"] as? [Any] {
                                completion(["items" : items], true)
                            }
                            else {
                                if let message: String = response["message"] as? String {
                                    TAUtility.showToast(message: message)
                                }
                                completion(nil, true)
                            }
                        } else {
                            if let message: String = response["message"] as? String {
                                TAUtility.showToast(message: message)
                            }
                            completion(nil, false)
                        }
                    }
                    else {
                        TAUtility.showToast(message: "Operation Failed. Please check youyr internet connection and try again")
                        completion(nil, false)
                    }
                } else {
                    completion(nil, false)
                }
            } else {
                completion(nil, false)
            }
        }
    }

    
}

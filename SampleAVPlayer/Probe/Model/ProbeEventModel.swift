//
//  ProbeEventModel.swift
//  SampleAVPlayer
//
//  Created by Saurabh Rode on 21/01/22.
//

import Foundation

// MARK: - ProbeEvent
struct ProbeEventModel: Codable {
    var event: EventModel?

    enum CodingKeys: String, CodingKey {
        case event = "event"
    }
}

// MARK: - Event
struct EventModel: Codable {
    
    var version: String = "1.0.0"
    var sdkVersion: String =  "1.0.0"
    var player: String = "AVPlayer"
    var platform: String = "iOS"
    var deviceType: String = "Mobile"
    var manufacturer: String = "Apple"
    var model: String = UIDevice.current.model
    var udid: String = UIDevice.current.identifierForVendor?.uuidString ?? ""
    var networkType: String = PINGModel.getConnectionType()
    var playerApp: String = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String ?? "Tata Sky Binge"
    var cdn: String?
    var ip: String?
    var provider: String?
    var ueid: String?
    var sessionID: String?
    var timestamp: Int?
    var playbackPosInSEC: Int?
    var videoID: String?
    var assetDuration: Int?
    var frameRate: Int?
    var aCodec: String?
    var vCodec: String?
    var bitrate: Int?
    var resolution: String?
    var throughput: Int?
    var has: String?
    var drm: String?
    var live: String?
    var event: String?
    var eventData: EventData?
    
    init(sessionID:String,playerApp: String ,provider: String?,videoID:String?,has:String) {
        self.sessionID = sessionID
        self.playerApp = playerApp
        self.provider = provider
        self.videoID = videoID
        self.has = has
    }

    enum CodingKeys: String, CodingKey {
        case version = "version"
        case sdkVersion = "sdkVersion"
        case player = "player"
        case playerApp = "playerApp"
        case cdn = "cdn"
        case ip = "ip"
        case provider = "provider"
        case ueid = "ueid"
        case udid = "udid"
        case platform = "platform"
        case deviceType = "deviceType"
        case manufacturer = "manufacturer"
        case model = "model"
        case networkType = "networkType"
        case sessionID = "sessionId"
        case timestamp = "timestamp"
        case playbackPosInSEC = "playbackPosInSec"
        case videoID = "videoId"
        case assetDuration = "assetDuration"
        case frameRate = "frameRate"
        case aCodec = "aCodec"
        case vCodec = "vCodec"
        case bitrate = "bitrate"
        case resolution = "resolution"
        case throughput = "throughput"
        case has = "has"
        case drm = "drm"
        case live = "live"
        case event = "event"
        case eventData = "eventData"
    }
}




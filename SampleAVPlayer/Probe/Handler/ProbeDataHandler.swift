//
//  ProbeInterfaceDataHandler.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 28/09/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
import UIKit


protocol ProbeProtocol:AnyObject{
    func setFrameRate(isEvent:Bool)
    func setPlayBackPostion(isEvent:Bool)
    func setCodecType(isEvent:Bool)
    func setDRMType(isEvent:Bool)
    func setAssetDuration(isEvent:Bool)
    func setBitrate(isEvent:Bool)
    func setThroughPut(isEvent:Bool)
    func setResolution(isEvent:Bool)
    func setLiveVideo(isEvent: Bool)
    func setIP(isEvent: Bool)
    func setUeid(isEvent:Bool)
    func setDurationOfPlayBack(isEvent:Bool)
    func setTimeStamp(isEvent: Bool)
}


final class ProbeDataHandler:NSObject {
    
    var syncData: [AnyEncodable] = []
    var probePingDataSource:ProbePingModel = ProbePingModel()
    var probeEventDataSource:ProbeEventModel = ProbeEventModel()
    var timer:Timer?
    var service:ProbeServiceHandler = ProbeServiceHandler()
    var shouldSyncData = true
    weak var delegate:ProbeProtocol?
    
    override init() {
        super.init()
        self.syncWithServerWithTimeInterval(timeInterval: 5)
    }
    
    func cleanDataSource(){
      
    }
    
    func setDataBeforePingToServer(isEvent:Bool){
        delegate?.setFrameRate(isEvent: isEvent)
        delegate?.setPlayBackPostion(isEvent: isEvent)
        delegate?.setCodecType(isEvent: isEvent)
        delegate?.setDRMType(isEvent: isEvent)
        delegate?.setAssetDuration(isEvent: isEvent)
        delegate?.setBitrate(isEvent: isEvent)
        delegate?.setThroughPut(isEvent: isEvent)
        delegate?.setResolution(isEvent: isEvent)
        delegate?.setIP(isEvent: isEvent)
        delegate?.setLiveVideo(isEvent: isEvent)
        delegate?.setUeid(isEvent: isEvent)
        delegate?.setDurationOfPlayBack(isEvent: isEvent)
        delegate?.setTimeStamp(isEvent: isEvent)
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
    
    func formatAndSaveData<T:Encodable>(rawData:T){
        do {
    
            let data = try JSONEncoder().encode(AnyEncodable(rawData))
            print("# formatAndSaveData \(String(decoding: data, as: UTF8.self))")
            
            let paramData = convertStringToDictionary(text: (String(decoding: data, as: UTF8.self))) ?? [:]
      
            ProbeServiceHandler().CallApiService(paramDict: paramData, urlEndpoint: URLEndpoint.analysisURl, requestType: .POST, dataModelType: PINGModel.self) {[weak self] (info, success) in
                if success {

                }
            }
                if shouldSyncData{

                 //   self.service.postCode(param: data)
                    //self.cleanDataSource()
                }
         
          //  }
           
        
        } catch {
            print(error)
        }
    }
    
    func syncWithServerWithTimeInterval(timeInterval:TimeInterval){
        self.timer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true) {[weak self] _ in
            if ((self?.shouldSyncData) != nil){
                self?.setDataBeforePingToServer(isEvent: false)
                self?.formatAndSaveData(rawData: self!.probePingDataSource)
            }
        }
    }
    
    func syncEventsWithServer(){
        self.setDataBeforePingToServer(isEvent: true)
        self.formatAndSaveData(rawData: self.probeEventDataSource)
    }
}


    
